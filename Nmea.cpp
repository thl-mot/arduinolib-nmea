/*
 * Nmea.cpp
 *
 *  Created on: 13.11.2018
 *      Author: thomas
 */

#include "Nmea.h"

Nmea::Nmea() {
	// TODO Auto-generated constructor stub
	valid=false;
	buffer="";
	gpgga="";
}

Nmea::~Nmea() {
	// TODO Auto-generated destructor stub
}

double getLat( String nmea, String northSouth) {
	double deg= atof( nmea.substring(0, 2).c_str());
	double min= atof( nmea.substring(2).c_str());
	if (northSouth.equals("N")) {
		return (deg+min/60.0);
	} else {
		return -1.0*(deg+min/60.0);
	}
}

double getLon( String nmea, String eastWest) {
	double deg= atof( nmea.substring(0, 3).c_str());
	double min= atof( nmea.substring(3).c_str());
	if (eastWest.equals("E")) {
		return (deg+min/60.0);
	} else {
		return -1.0*(deg+min/60.0);
	}
}

bool Nmea::handle( int c) {
	if (valid && c!=10 && c!=13) {
		buffer=buffer+(char)c;
	}
	if (c==13) {
		if ( buffer.startsWith("$GPGGA")) {
			gpgga=buffer;
			int k1= buffer.indexOf(',');
			int k2= buffer.indexOf(',', k1+1);
			int k3= buffer.indexOf(',', k2+1);
			int k4= buffer.indexOf(',', k3+1);
			int k5= buffer.indexOf(',', k4+1);
			int k6= buffer.indexOf(',', k5+1);
			int k7= buffer.indexOf(',', k6+1);
			int k8= buffer.indexOf(',', k7+1);
			int k9= buffer.indexOf(',', k8+1);
			int k10= buffer.indexOf(',', k9+1);
			int k11= buffer.indexOf(',', k10+1);

			String timeStr= buffer.substring( k1+1, k2);
			String latStr= buffer.substring( k2+1, k3);
			String northSouthStr= buffer.substring(k3+1, k4);
			String lonStr= buffer.substring( k4+1, k5);
			String eastWestStr= buffer.substring( k5+1, k6);
			String satCountStr= buffer.substring(k7+1, k8);

			String heightStr= buffer.substring( k9+1, k10);
			String heightScaleStr= buffer.substring( k10+1, k11);

			if (latStr.length()>0 && lonStr.length()>0) {
				lat= getLat( latStr, northSouthStr);
				lon= getLon( lonStr, eastWestStr);
				height= atof( heightStr.c_str());
				sateliteCount= atoi( satCountStr.c_str());
			} else {
				sateliteCount=0;
			}

//			Serial.println( "Lat: "+String(lat, 5));
//			Serial.println( "Lon: "+String(lon,5));
//			Serial.println( "H: "+String(height,2));
//			Serial.println( "Sat: "+String(sateliteCount));
		} else if ( buffer.startsWith("$GPRMC")) {
			gprmc=buffer;
			int k1= buffer.indexOf(',');
			int k2= buffer.indexOf(',', k1+1);
			int k3= buffer.indexOf(',', k2+1);
			int k4= buffer.indexOf(',', k3+1);
			int k5= buffer.indexOf(',', k4+1);
			int k6= buffer.indexOf(',', k5+1);
			int k7= buffer.indexOf(',', k6+1);
			int k8= buffer.indexOf(',', k7+1);
			int k9= buffer.indexOf(',', k8+1);
			int k10= buffer.indexOf(',', k9+1);
			int k11= buffer.indexOf(',', k10+1);

			String sogStr= buffer.substring(k7+1, k8);
			String cogStr= buffer.substring(k8+1, k9);
			dateTime= buffer.substring( k9+1, k10) + " "+buffer.substring(k1+1, k2);

			if (sogStr.length()>0 && cogStr.length()>0) {
				sog= atof( sogStr.c_str());
				cog= atof( cogStr.c_str());
			}

//			Serial.println( "Date/Time:"+dateTime);
//			Serial.println( "Speed/Course: "+String(sog,2)+"/"+String(cog,2));

		}
		buffer="";
		valid=true;

	}
	return true;
}
