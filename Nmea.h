/*
 * Nmea.h
 *
 *  Created on: 13.11.2018
 *      Author: thomas
 */

#ifndef NMEA_H_
#define NMEA_H_

#include <Arduino.h>

class Nmea {
private:
	bool valid;
	String buffer;

public:
	String gpgga;
	String gprmc;

	String dateTime;

	double lat;
	double lon;
	double height;
	int sateliteCount;

	double sog; // Speed over Ground
	double cog; // Course over Ground

	Nmea();
	virtual ~Nmea();

	bool handle( int c);
};

#endif /* NMEA_H_ */
